import zmq
import time
import multiprocessing


def task(id):
    ctx = zmq.Context()
    sock = ctx.socket(zmq.REP)
    sock.connect('tcp://localhost:6666')

    while True:
        msg = sock.recv_string()
        time.sleep(1)
        sock.send_string('%s: %s' % (id, msg))


pool = multiprocessing.Pool(10)
pool.imap_unordered(task, xrange(10))

while True:
    time.sleep(1)
