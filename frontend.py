import grequests
import datetime

requests = [grequests.get('http://localhost:8888/?id=' + str(i))
            for i in xrange(100)]

stat = []
try:
    for response in grequests.imap(requests, size=10):
        response.raise_for_status()
        stat.append(response.elapsed)
        print response.text, '[%s]' % response.elapsed
finally:
    total = sum(stat, datetime.timedelta())
    print '''
    Total time:   %s
    Count:        %s
    Concurrently: 10
    Average:      %s
    Min time:     %s
    Max time:     %s
''' % (total, len(stat), total / len(stat), min(stat), max(stat))
