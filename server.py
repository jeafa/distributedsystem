import zmq

from tornado import web
from tornado import gen
from tornado import ioloop


class MainHandler(web.RequestHandler):

    @gen.coroutine
    def get(self):
        ctx = zmq.Context.instance()
        sock = ctx.socket(zmq.REQ)
        sock.connect('tcp://localhost:5555')

        id = self.get_query_argument('id', '')
        sock.send_string(str(id))

        rv = None
        try:
            while rv is None:
                try:
                    rv = sock.recv_string(zmq.NOBLOCK)
                except zmq.Again:
                    yield gen.moment
        finally:
            sock.close()

        self.write(str(rv))
        self.finish()


app = web.Application([('/', MainHandler)])


if __name__ == '__main__':
    app.listen(8888)
    ioloop.IOLoop.instance().start()
