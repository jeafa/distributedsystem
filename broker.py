import zmq

ctx = zmq.Context.instance()

frontend = ctx.socket(zmq.ROUTER)
backend = ctx.socket(zmq.DEALER)

frontend.bind('tcp://*:5555')
backend.bind('tcp://*:6666')

poller = zmq.Poller()
poller.register(frontend, zmq.POLLIN)
poller.register(backend, zmq.POLLIN)


while True:
    socks = dict(poller.poll())

    if socks.get(frontend) == zmq.POLLIN:
        message = frontend.recv_multipart()
        backend.send_multipart(message)

    if socks.get(backend) == zmq.POLLIN:
        message = backend.recv_multipart()
        frontend.send_multipart(message)
